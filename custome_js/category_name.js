// Copyright (c) 2021, shree and contributors
// For license information, please see license.txt
var temp_cat;
var temp_cat_name;
var temp_sub_cat;
var temp_sub_cat_name;

frappe.ui.form.on('Category Name', {

	before_load:function(frm){
		console.log("before_load");

		frappe.form.link_formatters['Category Type'] = function(value, doc) {
		if(value!==undefined)
		{
			var temp_int=value.slice(1,value.length)
			if(/^\d+$/.test(temp_int))
			{
			temp_cat=value;
			console.log("category_type==",value)
			frappe.db.get_doc('Category Type', value)
		    .then(doc => {
		    	temp_cat_name=doc.ct;
		    	frm.set_value("category_type",temp_cat_name);
		    	return value;
		    		})
   				}


   		}
   		if(value==""){
   			console.log("else")
   			frm.set_value("category_type",temp_cat_name);
   		}

   		};

		frappe.form.link_formatters['Category Sub Type'] = function(value, doc) {
		if(value!==undefined)
		{
			var temp_int=value.slice(1,value.length)
			if(/^\d+$/.test(temp_int))
			{
			temp_sub_cat=value;
			console.log("category_sub_type==",value)
			frappe.db.get_doc('Category Sub Type', value)
		    .then(doc => {
		    	temp_sub_cat_name=doc.cst;
		    	frm.set_value("category_sub_type",temp_sub_cat_name);
		    	// console.log(temp)
		    	return value
		    		})
   				}
   			}

		if(value==""){
   			console.log("else")
   			debugger;
   			frm.set_value("category_sub_type",temp_sub_cat_name);
   			}

   		}

	},
	before_save:function(frm){
		console.log("before save",temp_cat);
		frm.set_value("category_type",temp_cat);
		frm.set_value("category_sub_type",temp_sub_cat);
	}


});

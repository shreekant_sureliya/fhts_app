# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in fhts_app/__init__.py
from fhts_app import __version__ as version

setup(
	name='fhts_app',
	version=version,
	description='this app includes all doc for fhts',
	author='shree',
	author_email='shree@gmail.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)

# -*- coding: utf-8 -*-
# Copyright (c) 2021, shree and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class ProjectMaster(Document):
	def after_insert(self):

		create_area(self)
		frappe.msgprint(
		msg="Area Created",
		title='Task Status')



def create_area(self):
		area_doc=frappe.new_doc("Area")
		area_doc.area_name=self.project_name
		area_doc.is_group=1
		area_doc.insert()
		frappe.db.commit()
		last_name=frappe.get_doc("Area",area_doc.name)
		change_name(last_name.name,self.project_name)
	# if {'name': 'Project'} not in frappe.get_list("Area Type"):
	# 		print("-------------if-------------")
	# 		areatype_doc=frappe.new_doc("Area Type")
	# 		areatype_doc.area_type_name="Project"
	# 		areatype_doc.insert()
	# 		frappe.db.commit()

	# 		area_doc=frappe.new_doc("Area")
	# 		area_doc.area_name=self.project_name
	# 		area_doc.area_type="Project"
	# 		area_doc.is_group=1
	# 		area_doc.insert()
	# 		frappe.db.commit()
	# 		last_name=frappe.get_doc("Area",area_doc.name)
	# 		change_name(last_name.name,self.project_name)

	# else:
	# 	print("-----------else------------")
		# area_doc=frappe.new_doc("Area")
		# area_doc.area_name=self.project_name
		# area_doc.area_type="Project"
		# area_doc.is_group=1
		# area_doc.insert()
		# frappe.db.commit()
		# last_name=frappe.get_doc("Area",area_doc.name)
		# change_name(last_name.name,self.project_name)


# @frappe.whitelist()
# def after_insert(doc_s):
#         create_area(doc_s)
#         frappe.msgprint(
#             msg="Area Created",
#             title='Task Status')

@frappe.whitelist()
def change_name(old_name,new_name):
	print("\n\n\n\n\n\n\n\n\n")
	qury='UPDATE tabArea SET name = "'+new_name+'" WHERE name ="'+old_name+'"'
	print(qury)
	frappe.db.sql(qury)
	frappe.db.commit()
	# print(frappe.db.get_value('Area', 'Project 9', 'name'))
	print("\n\n\n\n\n\n\n\n\n")
	return qury
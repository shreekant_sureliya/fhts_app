// Copyright (c) 2021, shree and contributors
// For license information, please see license.txt


frappe.ui.form.on('SOP Task Table', {


	onload:function(frm){
	frm.set_query("preceding_task", function() {
            return {
                filters: {'sop_name': frm.doc.sop_name }
            		}
          });

	},

	refresh: function(frm) {


		console.log("get_query filterd");
		frm.fields_dict['add_resource'].grid.get_field('category_sub_type').get_query = function(frm, cdt, cdn) {
			var child = locals[cdt][cdn];
			console.log(child);
			return{
				filters: {
					'type': child.category_type
				}
			}
		}

		frm.fields_dict['add_resource'].grid.get_field('category_name').get_query = function(frm, cdt, cdn) {
			var child = locals[cdt][cdn];
			console.log(child);
			return{
				filters: {
					'category_type':child.category_type,
					'category_sub_type': child.category_sub_type

				}
			}
		}


	}

});


frappe.ui.form.on('Add Resource', {

	category_name: function(frm,cdt,cdn) {
		var child = locals[cdt][cdn];
			

				frappe.db.get_doc('Category Name', child.category_name)
					    .then(doc => {

					    	console.log("lead_time=",doc.lead_time);

					    	// var grid_row = cur_frm.open_grid_row();
    						// var child = grid_row.doc;
    						// child.lead_time = do.lead_time;
    						
    						frappe.model.set_value(cdt,cdn,"lead_time",doc.lead_time);
    						frappe.model.set_value(cdt,cdn,"rate",doc.rate);
					   
					    })
	}

});










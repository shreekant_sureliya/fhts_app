// Copyright (c) 2021, shree and contributors
// For license information, please see license.txt

frappe.ui.form.on('Product Master', {
	refresh: function(frm) {

		console.log("filter");
		
		frm.set_query("category_sub_type", function() {
			return {
				filters: {'type': frm.doc.category_type}
			}
    	});

    	frm.set_query("category_name", function() {
			return {
				filters: {
					'category_type':frm.doc.category_type,
					'category_sub_type': frm.doc.category_sub_type
				}
			}
    	});

	}
});

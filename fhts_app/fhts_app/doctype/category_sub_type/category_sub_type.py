# -*- coding: utf-8 -*-
# Copyright (c) 2021, shree and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
# import frappe
from frappe.model.document import Document

class CategorySubType(Document):
	def autoname(self):
		self.name=self.category_sub_type_name+"←"+self.type
	

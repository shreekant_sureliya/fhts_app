// Copyright (c) 2021, shree and contributors
// For license information, please see license.txt


frappe.ui.form.on('Category Name', {

	setup:function(frm){
		console.log("this");

		frm.set_query("category_sub_type", function() {
			return {
				filters: {'type': frm.doc.category_type}
			}
		});

		},
	after_save(frm){
		setTimeout(() => {  frappe.set_route('Form', "Category Name", frm.doc.reference_name); }, 200);
		
	},

});

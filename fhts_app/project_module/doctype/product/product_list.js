frappe.listview_settings['Product'] = {
   hide_name_column: true,
    refresh: function(list){
    	console.log("on refresh");
    	var project_name=cur_list.filters[0][3]
		var area_type=cur_list.filters[1][3]
		var area_name=cur_list.filters[2][3]
		if(area_name.includes("%")){
			area_name=area_name.substring(1,(area_name.length-1))
		}
		list.page.set_primary_action(__("Product"), function() 
		{
			console.log("set_primary_action")
			frappe.route_options = {
				"project_name":project_name,
				"area_type": area_type,
				"area_name":area_name
			};
			frappe.new_doc("Product");
		})
    },

}
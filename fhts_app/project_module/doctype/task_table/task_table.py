# -*- coding: utf-8 -*-
# Copyright (c) 2021, shree and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import datetime


class TaskTable(Document):
	def autoname(self):
# select a project name based on customer
		prefix = "{}→{}→{}".format(self.area_name,self.sop_name,self.task_title)

		self.name = prefix
	# def on_change(self):

		# print("doc value is changeedddddddddddddddddddddddddddddddddddddddddd")
		# update_dependent_tasks(self)

@frappe.whitelist()
def create_list(area):
	lst=["select SOP"]
	doc_area=frappe.get_doc("Area",area)
	# doc_area_type=doc_area.area_type
	# doc=frappe.get_doc("Area Type",doc_area_type)
	name=doc_area.get_all_children("add_sop")
	for data in name:
		lst.append(data.sop_name)
	return lst

@frappe.whitelist()
def update_dependent_tasks(docname):
	doc=frappe.get_doc("Task Table",docname)
	start_date=doc.end_date
	docs=frappe.get_list("Task Table",filters={"pre_task":docname})
	count=0
	for doc_name in docs:
		count+=1
		doc=frappe.get_doc("Task Table",doc_name)
		doc.start_date=start_date
		doc.end_date=start_date+datetime.timedelta(days=int(doc.duration))
		doc.save()
		print("Docnameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee=,",doc_name.name)
		update_dependent_tasks(doc_name.name)

	frappe.db.commit()
	return count
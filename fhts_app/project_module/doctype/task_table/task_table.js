// Copyright (c) 2021, shree and contributors
// For license information, please see license.txt
function update_dependent_tasks(frm){
	frappe.call({
        method: "fhts_app.project_module.doctype.task_table.task_table.update_dependent_tasks",
        args:{
            docname:frm.docname            
        },
        callback:function(r){
            console.log(r.message)
            frappe.show_alert({
                message:__('Updated Dependent Tasks '+r.message),
                indicator:'green'
            }, 5);

        } 
    })    
}


frappe.ui.form.on('Task Table', {

	duration:function(frm){
        console.log("ducation changed");
		if(frm.doc.start_date!=undefined){
            frm.set_value("end_date",frappe.datetime.add_days(frm.doc.start_date,frm.doc.duration))
        }
		
	},

    preceding_sop:function(frm){
        console.log("preceding_sop");
        if (frm.doc.preceding_sop!=undefined)
        {
            var sop=frm.doc.preceding_sop.split("→")
            var sop_name=sop[sop.length-1]
            frm.set_query("pre_task", function() {
            return {
                filters: {
                'area_name': frm.doc.preceding_area,
                'sop_name':sop_name}
                }
            });
        }
    },

    onload_post_render:function(frm){
    console.log("onload_post_render");
        if (frm.doc.preceding_sop!=undefined)
        {
            var sop=frm.doc.preceding_sop.split("→")
            var sop_name=sop[sop.length-1]
            frm.set_query("pre_task", function() {
            return {
                filters: {
                'area_name': frm.doc.preceding_area,
                'sop_name':sop_name}
                }
            });
        }

        if (frm.doc.preceding_area!=undefined)
        {
            
            frm.set_query("preceding_sop", function() {
            return {
                filters: {
                    'area_name': frm.doc.preceding_area
                    }
                }
            });
        }
    },
    after_save:function(frm){
        update_dependent_tasks(frm);
    }


});


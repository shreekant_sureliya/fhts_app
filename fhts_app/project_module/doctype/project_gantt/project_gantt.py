# -*- coding: utf-8 -*-
# Copyright (c) 2021, shree and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Project_Gantt(Document):
	pass

@frappe.whitelist()
def get_task_list(area,sop):
    if sop!="0":
        temp=sop.split("→")
        if len(temp)>1:        
            sop_name=temp[-1]
            task_list=[]
            tasks=frappe.get_list("Task Table",filters={"sop_name":sop_name},order_by="task_no")
            for task in tasks:
                doc=frappe.get_doc("Task Table",task.name)
                temp={"id":doc.name,"name":doc.task_title,"start":doc.start_date.strftime("%y-%m-%d"),"end":doc.end_date.strftime("%y-%m-%d"),"dependencies":doc.pre_task,"progress": 100}
                task_list.append(temp)
            return task_list
    else:
        task_list=[]
        tasks=frappe.get_list("Task Table",filters={"area_name":area},order_by="task_no")
        for task in tasks:
            doc=frappe.get_doc("Task Table",task.name)
            temp={"id":doc.name,"name":doc.task_title,"start":doc.start_date.strftime("%y-%m-%d"),"end":doc.end_date.strftime("%y-%m-%d"),"dependencies":doc.pre_task,"progress": 100}
            task_list.append(temp)
        return task_list
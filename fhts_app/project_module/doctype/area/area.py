# -*- coding: utf-8 -*-
# Copyright (c) 2021, shree and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import publish_progress
from frappe.utils.nestedset import NestedSet
import datetime

class Area(NestedSet):
    
    # def after_insert(self):
        # no_of_task=create_tasks(self)
        # msg=str(no_of_task)+" Tasks created"
        # frappe.msgprint(
        #     msg=msg,
        #     title='Task Status')


    def autoname(self):
        if self.parent_area:
        	 self.name=self.parent_area+"→"+self.area_name
        else:
        	self.name=self.area_name

    def save(self, *args, **kwargs):
        
        no_of_task=create_tasks(self)
        msg=str(no_of_task)+" Tasks created"
        frappe.msgprint(
            msg=msg,
            title='Task Status')

        super().save(*args, **kwargs)


def get_date(doc):
    print(doc.task_title)
    if doc.pre_task:
        doc_pre=frappe.get_doc("Task Table",doc.pre_task)
        print("doc with pre task")
        if not doc_pre.end_date:
            print("date not found")
            _,start_date=set_date(doc_pre)            
            day,month,year=start_date.split("-")
            temp_start_date=datetime.datetime(int(day),int(month),int(year))
            temp_end_date=temp_start_date+datetime.timedelta(days=int(doc.duration))
            end_date=temp_end_date.strftime("%y-%m-%d")
            return [start_date,end_date]
        else:
            temp_start_date=doc_pre.end_date
            
#             day,month,year=start_date.split("-")
#             temp_start_date=datetime.datetime(int(day),int(month),int(year))
            temp_end_date=temp_start_date+datetime.timedelta(days=int(doc.duration))
            start_date=temp_start_date.strftime("%y-%m-%d")    
            end_date=temp_end_date.strftime("%y-%m-%d")
            return [start_date,end_date]
            



    else:
        t_start_date=frappe.get_doc("Project Master",doc.project_name).expected_start_date
        start_date =t_start_date.strftime("%y-%m-%d")
        temp_end_date=t_start_date+datetime.timedelta(days=int(doc.duration))
        end_date=temp_end_date.strftime("%y-%m-%d")
        print("else",start_date,end_date)
        return [start_date,end_date]

# doc=frappe.get_doc('Task Table','Adelphi→F.F→SOP00002.Green Field Activity & Project Pre-Design SOP→Design')

def set_date(doc):
    start_date,end_date=get_date(doc)
    doc.start_date=start_date
    doc.end_date=end_date
    doc.save()
    return start_date,end_date


# @frappe.whitelist()
# def get_name(area_doc):
# 	doc=frappe.get_doc("Area",area_doc.parent_area)
# 	name_list=[doc.area_name]
# 	while doc.parent_area:
# 		doc=frappe.get_doc("Area",doc.parent_area)
# 		name_list.append(doc.area_name)
# 	print("name_list=",name_list)
# 	final_name=""
# 	for name in name_list:
# 		final_name=name+"→"+final_name
# 	return final_name+area_doc.area_name

# def get_date(duration,pre_task,project_name):
    
#     if pre_task:
#         doc=frappe.get_doc("Task Table",pre_task)
#         start_date=doc.end_date
#         day,month,year=start_date.split("-")
#         temp_start_date=datetime.datetime(int(day),int(month),int(year))
#         temp_end_date=temp_start_date+datetime.timedelta(days=duration)
#         end_date=temp_end_date.strftime("%y-%m-%d")
#         print("if")
#         return [start_date,end_date]
#     else:
#         t_start_date=frappe.get_doc("Project Master",project_name).expected_start_date
#         start_date =t_start_date.strftime("%y-%m-%d")
#         temp_end_date=t_start_date+datetime.timedelta(days=duration)
#         end_date=temp_end_date.strftime("%y-%m-%d")
#         print("else")
#         return [start_date,end_date]

def get_task_names(sop_name):
    task_list=frappe.get_list("SOP Task Table",filters={'sop_name':sop_name})
    task_list.reverse()
    return task_list

def task_table_entry(project_name,area_name,task_doc):
    if not frappe.db.exists('Task Table', area_name+"→"+task_doc.sop_name+"→"+task_doc.task_title):
        new_doc=frappe.new_doc("Task Table")
        new_doc.project_name    = project_name
        new_doc.area_name       = area_name
        new_doc.sop_name        = task_doc.sop_name
        new_doc.task_no         = task_doc.task_no
        new_doc.task_title      = task_doc.task_title
        new_doc.duration        = int(task_doc.duration)
        new_doc.preceding_area  = area_name
        new_doc.preceding_sop   = area_name+"→"+task_doc.sop_name
        if task_doc.preceding_task:
            new_doc.pre_task    = area_name+"→"+task_doc.preceding_task
            
        new_doc.insert(ignore_links=True)
        frappe.db.commit()
        print("New Doc Name",new_doc.name)
        return True
    else:
        return None

def add_sop_in_list(area_sop_list,area_name):
    for sop_doc in area_sop_list:
        if not frappe.db.exists("Area SOP List",area_name+"→"+sop_doc.sop_name):    
            print(sop_doc.sop_name)
            doc=frappe.new_doc("Area SOP List")
            doc.area_name       = area_name
            doc.sop_name        = sop_doc.sop_name
            doc.save()
            frappe.db.commit()
    
def create_tasks(doc):
    total_created_task=0
    project_name=doc.project_name
    area_name=doc.name
    area_sop_list=doc.add_sop
    
    add_sop_in_list(area_sop_list,area_name)
    if area_sop_list:  
        for sop_doc in area_sop_list:
            print(sop_doc.sop_name)
             
            sop_tasks=get_task_names(sop_doc.sop_name)
            
            for task_name in sop_tasks:
                task_doc=frappe.get_doc("SOP Task Table",task_name)
                if task_table_entry(project_name,area_name,task_doc):
                    total_created_task+=1
        docs=frappe.get_list('Task Table',filters={"sop_name":sop_doc.sop_name})
        for doc_name in docs:
            doc=frappe.get_doc('Task Table',doc_name.name)
            print("get_date return=",set_date(doc))
            
        return total_created_task

    else:
        return 0




# #for bulk area generate
# @frappe.whitelist()
# def Create_child(node,parent):

    
#     expa=get_expandable(tree,node)

#     data=creat_docu(node,parent,expa)


#     print("expandable is =",expa)



#     if expa==1:
#         temp_list=get_child_docs(tree,node)
#         parent=data
#         print("parent is ="+parent)

#         for ch in temp_list:
#             Create_child(ch,parent)


# def get_child_docs(tree,node):
#     temp=tree.get_children("Area",node)
#     node_list=[]
#     for node in temp:
#         node_list.append(node.value)
#     return node_list
#     print(node_list)

# def creat_docu(node,parent,expa):

    
#     doc = frappe.new_doc('Area')
    
#     doc.area_name=frappe.get_doc("Area",node).area_name+"_3"
#     doc.parent_area=parent
#     doc.area_type="open_area" 
#     doc.is_group=expa
#     doc.area_start_date=frappe.get_doc("Area",node).area_start_date
#     doc.area_complete_date=frappe.get_doc("Area",node).area_complete_date
#     doc.project_name=frappe.get_doc("Area",node).project_name
#     doc.len=frappe.get_doc("Area",node).len
#     doc.wid=frappe.get_doc("Area",node).wid
#     doc.hei=frappe.get_doc("Area",node).hei
#     doc.ttl=frappe.get_doc("Area",node).ttl
#     doc.unit=frappe.get_doc("Area",node).unit
#     doc.comments=frappe.get_doc("Area",node).comments
#     doc.bulk_generated=frappe.get_doc("Area",node).bulk_generated
#     doc.star_marked=frappe.get_doc("Area",node).star_marked

#     my_str=frappe.get_doc("Area",node).whole_name
#     whole_n=my_str.replace(frappe.get_doc("Area",node).area_name,doc.area_name)
#     doc.whole_name=whole_n

#     doc.task_created=frappe.get_doc("Area",node).task_created
#     doc.insert()
#     frappe.db.commit()
#     create_tasks(doc.name)
#     # print("\n\n\n\n\n\n\n\n\n     create document is"+doc.area_name +" "+parent+" ")

#     #for allocate organization
#     # doc_list=frappe.get_list("Allocate Organization",filters={'project_name':parent,'area_name':frappe.get_doc("Area",node).area_name},fields=['name','area_name'])
#     # for dc in doc_list:
#     #     doc=frappe.get_doc("Allocate Organization",dc.name)
#     #     demo = frappe.new_doc('Allocate Organization')
#     #     demo.project_name=doc.project_name
#     #     demo.area_type=doc.area_type
#     #     demo.area_name=doc.area_name
#     #     demo.sop_name=doc.sop_name
#     #     demo.task_name=doc.task_name
#     #     demo.resource_type=doc.resource_type
#     #     demo.resource_sub_type=doc.resource_sub_type
#     #     demo.resource_name=doc.resource_name
#     #     demo.insert()
#     #     frappe.db.commit()
#     #     print("\n\n\n\n = data inserted...")

#     return doc.name

# def get_expandable(tree,node):
#     print("expandable is  ====================="+node)
#     if len(tree.get_children("Area",node)) > 0:
#         return 1
#     else:
#         return 0
var options_list;
var default_project="Project 1"
var count=2;
function waiting_fuction(){
	console.log("waiting_fuction");
}


function add_option(item, select_elemet) {
        // console.log(item,!in_list(options_list,item))
    if(item!=default_project){
        var element = document.createElement("option");
        console.log("item=",item)
        element.setAttribute("value", item);
        element.innerText=item;
        select_elemet[0].appendChild(element);
     }
}

function get_name(node)
{
    var final_name=node.label
    count=0;
    while(node.is_root==undefined){
        final_name=node.parent_label+"→"+final_name
        node=node.parent_node
        console.log(count++,final_name)
    }
    return final_name+"→"
}

//bulk area function
function bulk_dialog_fun(node){



    let bulk_dialog = new frappe.ui.Dialog({
         title: 'Bulk Area Gnenerate '+node.label,
                 fields: [

                 {
                     "fieldname": "column_break_1",
                     "fieldtype": "Column Break"
                 },


                    {
                        label: 'Number Of Area',
                        fieldname: 'number_of_area',
                        fieldtype: 'Data',
                   
                    },

                    {
                     "fieldname": "section_break_3",
                     "fieldtype": "Section Break"
                    },

                    {
                        label: 'Create Child Area',
                        fieldname: 'create_child_area',
                        fieldtype: 'Check',

                    },

                   


            ],
            primary_action_label: 'Save',
                 primary_action(values) {
                

                                var area_name = node.label;
                                var parent = node.parent_label;
                                var noa = values.number_of_area;



                                if(parent==null || node.is_root==true)
                                {
                                    console.log("parent is not clone")

                                }
                                else
                                {
                                        //  frappe.call({
                                        //     method: "fhts_app.project_module.doctype.area.area.Create_child",
                                        //     args:{
                                        //         node:area_name,
                                        //         parent:parent
                                                
                                        //     },
                                        //     callback:function(r){
                                        //         console.log(r.message)
                                        //     } 
                                        // }) 
                                }

                               




                bulk_dialog.hide();
            }


            


        });

    bulk_dialog.show();
    console.log("bulk area");
    
}

function get_sop_fields(node,dlg){
    var sop_fields=[]
    frappe.db.get_doc('Area', node)
    .then(doc => {
        console.log(doc)

        var sop_list=doc.add_sop

        for (var i = 0; i < sop_list.length; i++) {
            dlg.fields.push(
            {
                label: sop_list[i].sop_name,
                fieldname: i,
                fieldtype: 'Button',
                // sop_list[i]
            })
        }
        let sop_pop = new frappe.ui.Dialog(dlg);
        sop_pop.show();
    })
}

function create_dialog(node){
    console.log("create_dialog",node.title)
    // console.log(node.title);

    frappe.db.get_doc('Area', node.label)
    .then(doc => {

        var area_status=doc.area_status
        var start_pause_lable;

        if((area_status=="Pending to Start")||(area_status=="Stopped")) 
        {
            start_pause_lable="Start"
        }
        else{
            start_pause_lable="Stop/Pause"
        }
        console.log(area_status)
    

    let d = new frappe.ui.Dialog({
    title: 'Options',
    fields: [
            {
                // label: 
                fieldname: 'status',
                fieldtype: 'Data',
                read_only: '1',
                default:'Status: '+area_status,
            },
            {
               "fieldname": "column_break_2",
               "fieldtype": "Column Break"
            },                    
            {
                label: start_pause_lable,
                fieldname: 'pause',
                fieldtype: 'Button',
                click:function(){
                    if(area_status=="Started"){
                        frappe.db.set_value("Area",node.label,"area_status","Stopped")
                        console.log("Stopped area",node.label);

                        area_status="Stopped"
                        d.fields[0].label='Status: '+area_status
                        d.fields[2].label="Start"

                        d.refresh()
                        
                    }
                    else{
                        frappe.db.set_value("Area",node.label,"area_status","Started")
                        console.log("Started area",node.label);

                        area_status="Started"
                        d.fields[0].label='Status: '+area_status
                        d.fields[2].label="Stop/Pause"
                        d.refresh()
                        // d.refresh()
                        // d.show()
                    }
                    
                }
            },
            {
                "fieldname": "section_break_1",
                "fieldtype": "Section Break"
            },
           {
                label: 'Allocate organization',
                fieldname: 'allocate_organization',
                fieldtype: 'Button',
                click:function(){
                    count++;
                    console.log("button inside allocate organization ",node.title);
                    frappe.db.get_doc('Area', node.label)
                    .then(doc => {


                        frappe.route_options = {
                        "project_name":cur_tree.root_node.label,
                        "area_name":node.label
                        };

                        frappe.set_route("List","Task Table");
                        // frappe.set_route("List","Allocate Organization");  
                        // frappe.set_route("Form","Allocate Organization","new-allocate-organization-"+count,);  
                        console.log(doc.area_type)

                    })

                    console.log("add_organization "+cur_tree.root_node.label);

                }
            },
            {
               "fieldname": "column_break_4",
               "fieldtype": "Column Break"
            },                   
            {        
                label: 'Allocate Product',
                fieldname: 'allocate_product',
                fieldtype: 'Button',
                click:function(){
                    console.log("allocate_product");

                    frappe.db.get_doc('Area', node.label)
                    .then(doc => {
                        frappe.route_options = {
                        "project_name":cur_tree.root_node.label,
                        "area_type": doc.area_type,
                        "area_name":node.title

                        };
                        frappe.set_route("List","Product");  
                        console.log(doc.area_type)

                    })
                }
            }, 
            {
               "fieldname": "column_break_7",
               "fieldtype": "Column Break"
            },
            {
                label: 'Release for Execution',
                fieldname: 'release_for_execution',
                fieldtype: 'Button',
                click:function(){
                    console.log("release_for_execution");
                    frappe.db.get_doc('Area', node.label)
                        .then(doc => {
                        frappe.route_options = {
                            "project_name":cur_tree.root_node.label,
                            "area_type": doc.area_type,
                            "area_name":node.title
                            };
                            frappe.set_route("Form","Attain Area SOP Task","new-attain-area-sop-task-1");  
                            console.log(doc.area_type)

                    })
                }
            },     

             {
                       "fieldname": "column_break_8",
                       "fieldtype": "Column Break"
                    },
                        {
                        label: 'Bulk Area',
                        fieldname: 'bulk_area',
                        fieldtype: 'Button',
                        click:function(){
                            bulk_dialog_fun(node);
                        }
                    },
            {
               "fieldname": "column_break_9",
               "fieldtype": "Column Break"
            },
            {
                label: 'Show Tasks',
                fieldname: 'task_table',
                fieldtype: 'Button',
                click:function(){
                    console.log("release_for_execution");
                    frappe.db.get_doc('Area', node.label)
                        .then(doc => {
                        frappe.route_options = {
                            "project_name":cur_tree.root_node.label,
                            "area_name":node.title
                            };
                            frappe.set_route("List","Task Table");  
                            console.log(doc.area_type)

                    })
                }
            },                      

        ],
        primary_action_label: 'Cancle',
        primary_action(values) {
            console.log(values);
            d.hide();
        }
    });

    d.show();
    })

}



frappe.provide("frappe.treeview_settings");
frappe.treeview_settings["Area"] = {
    get_tree_root: false,
		filters: [
		{
			fieldname: "project_name",
			fieldtype:"Select",
			options: ["Project 1"],
			label: __("project_options"),
			default: default_project


		}],
    post_render: function(treeview) {
        console.log("post_render")
    },
    refresh:function(treeview){
        console.log("refresh");
        treeview.reload_node(treeview.root_node);
    },
    onload: function(treeview){
    	
        console.log("#############Creating button inside tree view accounts_tree.js###############");

        frappe.treeview_settings['Area'].treeview = {};
        $.extend(frappe.treeview_settings['Area'].treeview, treeview);
        function get_company() {
            return treeview.page.fields_dict.company.get_value();
        }

        treeview.page.add_button(__("Release Work Order"),
        function() 
            {
                console.log("release_work_order");
            }
        )
        treeview.page.add_button(__("Material Purchase"),
        function() 
            {
                console.log("material_purchase");
            }
        )

        treeview.page.add_button(__("Add organisations"),
        function() 
            {
            
                console.log("add organisations called");
                frappe.route_options = {
                        "project_name":cur_tree.root_node.label,
                        };
                frappe.set_route("List","Project Organisation");

                
            
            }
        )

    	var count=0;
    	var select_elemet
    	while(true){
    		if(select_elemet==undefined)
    		{
    			select_elemet=document.getElementsByTagName("select");
    			setTimeout(waiting_fuction,1000);
    			console.log(count);
    			count++;
    		}
    		else{
    			break;
    		}
    	}
    	select_elemet[0].onchange=function(){
			console.log("changed value")
		}

		// frappe.call({
  //   		method: "temp_app.temp_app.doctype.area_master.area_master.get_projects",
  //   		callback:function(r)
  //           {
  //       	console.log("inside call",r.message)

  frappe.db.get_list('Project Master')
    .then(list => {
        console.log(list)
        
        for (var i = 0; i < list.length; i++) {
        
               add_option(list[i].name,select_elemet); 
           }   
        	

		document.getElementsByClassName("placeholder ellipsis text-extra-muted xs")[0].style="display: none;"
		
	})  
    	console.log("onload");
   },
   toolbar: [
        {
            label:__("Option"),
            condition: function(node) {
                return true;
            },
            click: function(node) {
                console.log("button clicked",node.title)
                create_dialog(node);
            },
            btnClass: "hidden-xs"
        },
        {
            label:__("Add Child"),
            condition: function(node) {
                return node.expandable;
            },
            click: function(node) {
                var area_name=get_name(node)
                frappe.route_options = {
                        // "whole_name":area_name,
                        "project_name":cur_tree.root_node.label,
                        "parent_area":node.label,
                        };

                console.log("button clicked of add child",node.title)
                frappe.new_doc("Area");
            },
            btnClass: "hidden-xs"
        },
        {
            label:__("Edit"),
            condition: function(node) {
                return true;
            },
            click: function(node) {
                frappe.set_route("Form","Area",node.label)
            },
            btnClass: "hidden-xs"
        },
    ],
    extend_toolbar: true
};

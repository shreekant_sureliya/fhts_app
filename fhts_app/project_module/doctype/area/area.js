// Copyright (c) 2021, shree and contributors
// For license information, please see license.txt

function set_volume(frm) {
	console.log("set_volume");
	var total_volume=frm.doc.len*frm.doc.wid*frm.doc.hei;	
	frm.set_value("total_volume",total_volume);
	frm.set_value("total_area","");
}

function set_area(frm){
	var total_area=frm.doc.len*frm.doc.wid;	
	console.log("set_area",total_area);
	frm.set_value("total_area",total_area);
	frm.set_value("total_volume","");
}

function hide_both(frm){
	console.log("hide_both");
	frm.set_value("total_area","");
	frm.set_value("total_volume","");	
}

function calculate_area(frm){
	var total_area;
	if((frm.doc.len!=undefined)&&(frm.doc.wid!=undefined)&&(frm.doc.hei!=undefined)&&(frm.doc.hei!=0))
	{
		set_volume(frm)

	}
	else if((frm.doc.len!=undefined)&&(frm.doc.wid!=undefined)&&(frm.doc.len!=0)&&(frm.doc.wid!=0)){
		set_area(frm)
	}
	else{
		hide_both(frm);
	}
}

frappe.ui.form.on('Area', {
	
	refresh: function(frm) {
		console.log("on refresh");
		// calculate_area(frm);

			frm.fields_dict['add_sop'].grid.get_field('sop_name').get_query = function(frm, cdt, cdn) {
			var child = locals[cdt][cdn];
			console.log(child);
			return{
				filters: {
					'sop_type': child.sop_type
				}
			}
		}

	},
	len:function(frm){
		console.log("changed lenght");
		calculate_area(frm);
	},
	wid:function(frm){
		console.log("changed width");	
		calculate_area(frm);
	},
	hei:function(frm){
		console.log("changed height");
		calculate_area(frm);	
	},
	area_name:function(frm){
		console.log("name =",frm.doc.area_name);
	},



});


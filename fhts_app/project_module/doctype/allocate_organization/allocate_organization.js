// Copyright (c) 2021, shree and contributors
// For license information, please see license.txt
var task_list;

function get_resource(task_name){
    if((task_name!= undefined)&&(task_name!="Select Task")){

        var task_count=(task_name.split(" ",1)-1)
        var cur_task=task_list[task_count];
        //category type
        console.log("cur_task=",cur_task);
        var ct=["ct_1","ct_2","ct_3","ct_4","ct_5","ct_6","ct_7","ct_8"];
        //category sub type
        var cst=["cst_1","cst_2","cst_3","cst_4","cst_5","cst_6","cst_7","cst_8"];
        //categoru name
        var cn=["cn_1","cn_2","cn_3","cn_4","cn_5","cn_6","cn_7","cn_8"];
        
        var resource=[]
        for (var i = 0; i < 8; i++) {
            if(cur_task[ct[i]]!=undefined)
            {
                resource.push({
                    ct:cur_task[ct[i]],
                    cst:cur_task[cst[i]],
                    cn:cur_task[cn[i]]
                })
            }
        }
        console.log(resource)
        return resource
    }

}

function remove_resource(frm){
    // frm.get_field("organization_name").grid.remove_all();
    if(cur_frm.doc.organization_name!=undefined){

        cur_frm.doc.organization_name.splice(0);
        cur_frm.refresh_fields()
    }

    // frappe.model.set_value(frm.doc.organization_name[0].doctype,frm.doc.organization_name[0].name,"category_type",null);
    // frappe.model.set_value(frm.doc.organization_name[0].doctype,frm.doc.organization_name[0].name,"category_sub_type",null);
    // frappe.model.set_value(frm.doc.organization_name[0].doctype,frm.doc.organization_name[0].name,"category_name",null);

}

function set_resource(frm,res){
    if(res.length!=undefined){

        // frappe.model.set_value(frm.doc.organization_name[0].doctype,frm.doc.organization_name[0].name,"category_type",res[0].ct);
        // frappe.model.set_value(frm.doc.organization_name[0].doctype,frm.doc.organization_name[0].name,"category_sub_type",res[0].cst);
        // frappe.model.set_value(frm.doc.organization_name[0].doctype,frm.doc.organization_name[0].name,"category_name",res[0].cn);

        // if(res.length>1)
        // {
        for (var i = 0; i < res.length; i++) {
            
            console.log("for loop",i)

            frm.add_child("organization_name",{
                category_type:res[i].ct,
                category_sub_type:res[i].cst,
                category_name:res[i].cn
                })
            
            }
        frm.refresh_fields();
    // }
        // frm.refresh_fields("organizaion_name")
    }
}


function remove_allocated_task(task_list){
    
}

function get_sop_names(frm){

    var sop_names=["Select SOP"]
    frappe.db.get_doc('Area Type', frm.doc.area_type)
    .then(doc => {
        console.log(doc)
        var sop_list=doc.add_sop

        for (var i = 0; i < sop_list.length; i++) {
            sop_names.push(sop_list[i].sop_name)
        	}
        frm.set_df_property('sop_name', 'options', sop_names);
		if(frm.doc.sop_name==undefined){frm.set_value("sop_name","Select SOP")}
		else{frm.refresh_field("sop_name")}        
    })
}

function get_task_names(frm){
    console.log("get task called")
    function waitForElement(){
    
    if(typeof frm.doc.sop_name !== "undefined"){
    var task_names=["Select Task"]
    console.log("defined sop_name=",frm.doc.sop_name)
    if(frm.doc.sop_name!="Select SOP"){
    
    frappe.db.get_doc('SOP Master', frm.doc.sop_name)
    .then(doc => {
        console.log("SOP Fetch",doc)
        task_list=doc.add_task

        for (var i = 0; i < task_list.length; i++) {
            task_names.push((i+1)+" "+task_list[i].task_title)
        	}

        // var filterd_task=remove_allocated_task(task_list);

        frm.set_df_property('task_name', 'options', task_names);
		if(frm.doc.task_name==undefined){frm.set_value("task_name","Select Task")}
		else{frm.refresh_field("task_name")}        
    })
    }
    else{
        console.log("running else",frm.doc.task_name,task_names)
        // if(frm.doc.task_name==undefined)||{
            frm.set_df_property('task_name', 'options', task_names);
            frm.set_value("task_name","Select Task")
            frm.refresh_field("task_name")
        // }  
    }
    return;
    }
    else{
        console.log("waiting for elemnt");
        setTimeout(waitForElement, 250);
        }
    }

    waitForElement();
}

frappe.ui.form.on('Allocate Organization', {


	before_load: function(frm) {
		if(frm.doc.area_type!=undefined){
		get_sop_names(frm);
		get_task_names(frm);
	}},
	sop_name:function(frm){
		
		console.log("selected function called",frm.doc.sop_name);
		get_task_names(frm);
		

	},
    task_name:function(frm){
            
            console.log("selected task",frm.doc.task_name);
            remove_resource(frm);
            var res=get_resource(frm.doc.task_name);
            console.log("res===",res);
            if(res!=undefined){    
                set_resource(frm,res);  
            }
        // debugger;
    },
    refresh:function(frm){
        $(".grid-add-row").hide();
    }
	
});
